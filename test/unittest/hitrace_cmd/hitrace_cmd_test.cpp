/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "hitrace_cmd.h"
#include "common_utils.h"

#include <iostream>
// #include <memory>
// #include <fstream>
// #include <string>
// #include <vector>
// #include <map>

// #include <unistd.h>
// #include <cstdio>
// #include <fcntl.h>
// #include "securec.h"

#include "hilog/log.h"
// #include "parameters.h"
#include <gtest/gtest.h>

using namespace OHOS::HiviewDFX::Hitrace;
using namespace testing::ext;
using OHOS::HiviewDFX::HiLog;

namespace {

#undef LOG_DOMAIN
#define LOG_DOMAIN 0xD002D33

#undef LOG_TAG
#define LOG_TAG "HitraceTest"

std::shared_ptr<OHOS::HiviewDFX::UCollectClient::TraceCollector> g_traceCollector;

class HitraceCmdTest : public testing::Test {
public:
    static void SetUpTestCase(void);
    static void TearDownTestCase(void);
    void SetUp();
    void TearDown() {}
};

void HitraceCmdTest::SetUp()
{
    /* SetUp CMD_MODE */
}

void HitraceCmdTest::SetUpTestCase()
{
    HILOG_ERROR("SetUpTestCase start.");
    setgid(SHELL_UID);
    g_traceCollector = OHOS::HiviewDFX::UCollectClient::TraceCollector::Create();
    if (g_traceCollector == nullptr) {
        HILOG_ERROR("error: traceCollector create failed, exit.");
        return;
    }
    (void)signal(SIGKILL, InterruptExit);
    (void)signal(SIGINT, InterruptExit);

    if (!IsTraceMounted()) {
        HILOG_ERROR("error: trace isn't mounted, exit.");
        return;
    }

    if (!InitAllSupportTags()) {
        return;
    }

    /* Open CMD_MODE */
}

void HitraceCmdTest::TearDownTestCase()
{
    /* Close CMD_MODE */
}

/**
 * @tc.name: GetTraceModeTest_001
 * @tc.desc: test GetTraceMode() service mode
 * @tc.type: FUNC
 */
HWTEST_F(HitraceCmdTest, GetTraceModeTest_001, TestSize.Level0)
{
    int argc = 7;
    char* argv[] = {"-t", "10", "sched", "app", "freq"};
    g_traceArgs.duration = 10;
    g_traceArgs.tags = {"sched", "app", "freq"};
    HILOG_ERROR("SetUpTestCase start.");
    ASSERT_TRUE(HandleRecordingShortRaw());
}

} // namespace